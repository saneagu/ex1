package ro.orangeStartIT;

public class HotelPrivat {
    private int Price;
    private String hotelName;
    private String hotelOwner;

//    public HotelPrivat () {
//        this.hotelName="n/a";
//        System.out.println("Attention! New entry without required details. Please correct!");
//    }

    public HotelPrivat (String newHotelName, String newHotelOwner, int newHotelPrice) {
        this.setHotelName(newHotelName);
        this.setHotelOwner(newHotelOwner);
        this.setHotelPrice(newHotelPrice);
        System.out.println("New hotel entry with the following details: Name: "+newHotelName+", Owner: "+newHotelOwner+", Daily rate:"+newHotelPrice+". Thank you for your input!");
    }

    public void setHotelName (String newHotelNameEntry) {
        this.hotelName=newHotelNameEntry;
    }

    public void setHotelOwner (String newHotelOwnerEntry) {
        this.hotelOwner=newHotelOwnerEntry;
    }

    public void setHotelPrice (int newHotelPriceEntry) {
        this.Price=newHotelPriceEntry;
    }

    public String getHotelName() {
        return this.hotelName;
    }

    public String getHotelOwner() {
        return this.hotelOwner;
    }

    public int getHotelPrice() {
        return this.Price;
    }

}
//old version made in class
/*public class Hotel {
    int price;
    String Owner;
    String HotelName;

    public Hotel(){
    }

    public void getPrice(){

    }

//    public void display(){
//        System.out.println();
}*/
