package ro.orangeStartIT;
public class TestHotel {

    public static void main(String[] args) {
        HotelPrivat H1 = new HotelPrivat("Pacific Phoenix Hotel","Edward Walter",75);
        HotelPrivat H2 = new HotelPrivat("Ivory Manor Hotel has","Christopher J. Nassetta",85);

        System.out.println("The hotel "+H1.getHotelName()+" has the owner "+H1.getHotelOwner()+".");
        System.out.println("The hotel "+H2.getHotelName()+" has the owner "+H2.getHotelOwner()+".");

    }
}